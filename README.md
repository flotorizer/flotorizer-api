# flotorizer-api

npm module to facilitate flotorizations with the user apiKey

## Usage

```javascript
const Flotorizer = require('flotorizer-api')

const flotorizerConfig = {
    apiKey = "&f1&2G19%MeqbwIPRTnSSx9jApW!BYER"
}

const flotorizer = new Flotorizer(flotorizerConfig)

flotorizer.it(payload, options).then()
flotorizer.them(payloads, options).then()
```